# LoRa Forklift Location

Forklift Location sensors are used to monitor the location and movement of forklifts.
For detailed information about these sensors, refer to
the [sensor's documentation](./Ease_EG-IoT_1149_UG_V2.15.2_UK_000-1.pdf).

## Overview

### Owners & Administrators

| Title       | Details                                                                                                            |
|-------------|--------------------------------------------------------------------------------------------------------------------|
| **Section** | [SCE-SAM-TG](https://phonebook.cern.ch/search?q=SCE-SAM-TG)                                                        |
| **E-group** | [lora-en-forklift-location-admin](https://groups-portal.web.cern.ch/group/lora-en-forklift-location-admin/details) |
| **People**  | [Clarisse Aubert](https://phonebook.cern.ch/search?q=Clarisse+Aubert)                                              |
|             | [Adrian Garcia Fernandez](https://phonebook.cern.ch/search?q=Adrian+Garcia+Fernandez)                              |
|             | [Nicolas Guilhaudin](https://phonebook.cern.ch/search?q=Nicolas+Guilhaudin)                                        |

### Kafka Topics

| Environment    | Topic Name                                                                                                                    |
|----------------|-------------------------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-forklift-location](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-forklift-location)                 |
| **Production** | [lora-forklift-location-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-forklift-location-decoded) |
| **QA**         | [lora-forklift-location](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-forklift-location)                 |
| **QA**         | [lora-forklift-location-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-forklift-location-decoded) |

### Configuration

| Title                        | Details                                                                                                      |
|------------------------------|--------------------------------------------------------------------------------------------------------------|
| **Application Name**         | lora-EN-forklift-location                                                                                    |
| **Configuration Repository** | [app-configs/lora-forklift-location](https://gitlab.cern.ch/nile/streams/app-configs/lora-forklift-location) |

### Technologies

- LoRa
- LTE-M