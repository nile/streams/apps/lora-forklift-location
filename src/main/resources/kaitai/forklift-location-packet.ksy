meta:
  id: forklift_location_packet
  endian: be
  title: LoRa Forklift Location packet
  # doc-ref: https://gitlab.cern.ch/nile/streams/apps/lora-forklift-location/-/blob/master/Ease_EG-IoT_1149_UG_V2.15.2_UK_000-1.pdf

seq:
  - id: frame_code
    type: u1
    doc: Frame code
  - id: payload
    type:
      switch-on: frame_code
      cases:
        132: start_frame
        128: stop_frame
        16: cyclic_frame
        _: unsupported_frame

types:
  start_frame:
    seq:
      - id: latitude_raw_hidden
        type: s4
        doc: Raw latitude value, divide by 100000 to get actual latitude.
      - id: longitude_raw_hidden
        type: s4
        doc: Raw longitude value, divide by 100000 to get actual longitude.
      - id: battery_voltage_raw_hidden
        type: u1
        doc: Raw battery voltage, convert to actual voltage by adding 200 and dividing by 100.
      - id: gnss_and_temperature_byte_hidden
        type: u1
        doc: Byte containing GNSS fix indicator and raw temperature.
      - id: speed_accuracy_input_states_byte_hidden
        type: u1
        doc: Byte containing speed, HDOP accuracy, and input states.
    instances:
      latitude:
        value: latitude_raw_hidden / 100000.0
      longitude:
        value: longitude_raw_hidden / 100000.0
      battery_voltage:
        value: (battery_voltage_raw_hidden + 200) / 100.0
      gnss_fix:
        value: (gnss_and_temperature_byte_hidden & 0x80) != 0
        doc: GNSS fix status; true if fix is valid.
      temperature:
        value: (gnss_and_temperature_byte_hidden & 0x7F) - 40
        doc: Temperature in Celsius, calculated from the byte value.
      speed:
        value: ((speed_accuracy_input_states_byte_hidden >> 4) & 0x0F) * 10
        doc: Speed in km/h, calculated from the upper 4 bits.
      accuracy:
        value: (speed_accuracy_input_states_byte_hidden >> 2) & 0x03
        doc: HDOP accuracy level derived from bits 3 and 2.
      input_state_2:
        value: (speed_accuracy_input_states_byte_hidden & 0x02) != 0
        doc: Input state 2, true if active.
      input_state_1:
        value: (speed_accuracy_input_states_byte_hidden & 0x01) != 0
        doc: Input state 1, true if active.

  stop_frame:
    seq:
      - id: latitude_raw_hidden
        type: s4
        doc: Raw latitude value, divide by 100000 to get actual latitude.
      - id: longitude_raw_hidden
        type: s4
        doc: Raw longitude value, divide by 100000 to get actual longitude.
      - id: battery_voltage_raw_hidden
        type: u1
        doc: Raw battery voltage, convert to actual voltage by adding 200 and dividing by 100.
      - id: gnss_and_temperature_byte_hidden
        type: u1
        doc: Byte containing GNSS fix indicator and raw temperature.
      - id: speed_accuracy_input_states_byte_hidden
        type: u1
        doc: Byte containing speed, HDOP accuracy, and input states.
    instances:
      latitude:
        value: latitude_raw_hidden / 100000.0
      longitude:
        value: longitude_raw_hidden / 100000.0
      battery_voltage:
        value: (battery_voltage_raw_hidden + 200) / 100.0
      gnss_fix:
        value: (gnss_and_temperature_byte_hidden & 0x80) != 0
        doc: GNSS fix status; true if fix is valid.
      temperature:
        value: (gnss_and_temperature_byte_hidden & 0x7F) - 40
        doc: Temperature in Celsius, calculated from the byte value.
      speed:
        value: ((speed_accuracy_input_states_byte_hidden >> 4) & 0x0F) * 10
        doc: Speed in km/h, calculated from the upper 4 bits.
      accuracy:
        value: (speed_accuracy_input_states_byte_hidden >> 2) & 0x03
        doc: HDOP accuracy level derived from bits 3 and 2.
      input_state_2:
        value: (speed_accuracy_input_states_byte_hidden & 0x02) != 0
        doc: Input state 2, true if active.
      input_state_1:
        value: (speed_accuracy_input_states_byte_hidden & 0x01) != 0
        doc: Input state 1, true if active.

  cyclic_frame:
    seq:
      - id: latitude_raw_hidden
        type: s4
        doc: Raw latitude value, divide by 100000 to get actual latitude.
      - id: longitude_raw_hidden
        type: s4
        doc: Raw longitude value, divide by 100000 to get actual longitude.
      - id: battery_voltage_raw_hidden
        type: u1
        doc: Raw battery voltage, convert to actual voltage by adding 200 and dividing by 100.
      - id: gnss_and_temperature_byte_hidden
        type: u1
        doc: Byte containing GNSS fix indicator and raw temperature.
      - id: speed_accuracy_input_states_byte_hidden
        type: u1
        doc: Byte containing speed, HDOP accuracy, and input states.
    instances:
      latitude:
        value: latitude_raw_hidden / 100000.0
      longitude:
        value: longitude_raw_hidden / 100000.0
      battery_voltage:
        value: (battery_voltage_raw_hidden + 200) / 100.0
      gnss_fix:
        value: (gnss_and_temperature_byte_hidden & 0x80) != 0
        doc: GNSS fix status; true if fix is valid.
      temperature:
        value: (gnss_and_temperature_byte_hidden & 0x7F) - 40
        doc: Temperature in Celsius, calculated from the byte value.
      speed:
        value: ((speed_accuracy_input_states_byte_hidden >> 4) & 0x0F) * 10
        doc: Speed in km/h, calculated from the upper 4 bits.
      accuracy:
        value: (speed_accuracy_input_states_byte_hidden >> 2) & 0x03
        doc: HDOP accuracy level derived from bits 3 and 2.
      input_state_2:
        value: (speed_accuracy_input_states_byte_hidden & 0x02) != 0
        doc: Input state 2, true if active.
      input_state_1:
        value: (speed_accuracy_input_states_byte_hidden & 0x01) != 0
        doc: Input state 1, true if active.

  unsupported_frame:
    seq:
      - id: unsupported_data
        size-eos: true
        doc: Unsupported data payload
