// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

package ch.cern.nile.app.generated;

import java.io.IOException;

import io.kaitai.struct.ByteBufferKaitaiStream;
import io.kaitai.struct.KaitaiStream;
import io.kaitai.struct.KaitaiStruct;

public class ForkliftLocationPacket extends KaitaiStruct {
    public static ForkliftLocationPacket fromFile(String fileName) throws IOException {
        return new ForkliftLocationPacket(new ByteBufferKaitaiStream(fileName));
    }

    public ForkliftLocationPacket(KaitaiStream _io) {
        this(_io, null, null);
    }

    public ForkliftLocationPacket(KaitaiStream _io, KaitaiStruct _parent) {
        this(_io, _parent, null);
    }

    public ForkliftLocationPacket(KaitaiStream _io, KaitaiStruct _parent, ForkliftLocationPacket _root) {
        super(_io);
        this._parent = _parent;
        this._root = _root == null ? this : _root;
        _read();
    }

    private void _read() {
        this.frameCode = this._io.readU1();
        switch (frameCode()) {
            case 132: {
                this.payload = new StartFrame(this._io, this, _root);
                break;
            }
            case 128: {
                this.payload = new StopFrame(this._io, this, _root);
                break;
            }
            case 16: {
                this.payload = new CyclicFrame(this._io, this, _root);
                break;
            }
            default: {
                this.payload = new UnsupportedFrame(this._io, this, _root);
                break;
            }
        }
    }

    public static class StartFrame extends KaitaiStruct {
        public static StartFrame fromFile(String fileName) throws IOException {
            return new StartFrame(new ByteBufferKaitaiStream(fileName));
        }

        public StartFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public StartFrame(KaitaiStream _io, ForkliftLocationPacket _parent) {
            this(_io, _parent, null);
        }

        public StartFrame(KaitaiStream _io, ForkliftLocationPacket _parent, ForkliftLocationPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }

        private void _read() {
            this.latitudeRawHidden = this._io.readS4be();
            this.longitudeRawHidden = this._io.readS4be();
            this.batteryVoltageRawHidden = this._io.readU1();
            this.gnssAndTemperatureByteHidden = this._io.readU1();
            this.speedAccuracyInputStatesByteHidden = this._io.readU1();
        }

        private Integer accuracy;

        /**
         * HDOP accuracy level derived from bits 3 and 2.
         */
        public Integer accuracy() {
            if (this.accuracy != null) {
                return this.accuracy;
            }
            int _tmp = (int) (((speedAccuracyInputStatesByteHidden() >> 2) & 3));
            this.accuracy = _tmp;
            return this.accuracy;
        }

        private Double batteryVoltage;

        public Double batteryVoltage() {
            if (this.batteryVoltage != null) {
                return this.batteryVoltage;
            }
            double _tmp = (double) (((batteryVoltageRawHidden() + 200) / 100.0));
            this.batteryVoltage = _tmp;
            return this.batteryVoltage;
        }

        private Boolean inputState1;

        /**
         * Input state 1, true if active.
         */
        public Boolean inputState1() {
            if (this.inputState1 != null) {
                return this.inputState1;
            }
            boolean _tmp = (boolean) ((speedAccuracyInputStatesByteHidden() & 1) != 0);
            this.inputState1 = _tmp;
            return this.inputState1;
        }

        private Integer temperature;

        /**
         * Temperature in Celsius, calculated from the byte value.
         */
        public Integer temperature() {
            if (this.temperature != null) {
                return this.temperature;
            }
            int _tmp = (int) (((gnssAndTemperatureByteHidden() & 127) - 40));
            this.temperature = _tmp;
            return this.temperature;
        }

        private Double latitude;

        public Double latitude() {
            if (this.latitude != null) {
                return this.latitude;
            }
            double _tmp = (double) ((latitudeRawHidden() / 100000.0));
            this.latitude = _tmp;
            return this.latitude;
        }

        private Double longitude;

        public Double longitude() {
            if (this.longitude != null) {
                return this.longitude;
            }
            double _tmp = (double) ((longitudeRawHidden() / 100000.0));
            this.longitude = _tmp;
            return this.longitude;
        }

        private Boolean gnssFix;

        /**
         * GNSS fix status; true if fix is valid.
         */
        public Boolean gnssFix() {
            if (this.gnssFix != null) {
                return this.gnssFix;
            }
            boolean _tmp = (boolean) ((gnssAndTemperatureByteHidden() & 128) != 0);
            this.gnssFix = _tmp;
            return this.gnssFix;
        }

        private Integer speed;

        /**
         * Speed in km/h, calculated from the upper 4 bits.
         */
        public Integer speed() {
            if (this.speed != null) {
                return this.speed;
            }
            int _tmp = (int) ((((speedAccuracyInputStatesByteHidden() >> 4) & 15) * 10));
            this.speed = _tmp;
            return this.speed;
        }

        private Boolean inputState2;

        /**
         * Input state 2, true if active.
         */
        public Boolean inputState2() {
            if (this.inputState2 != null) {
                return this.inputState2;
            }
            boolean _tmp = (boolean) ((speedAccuracyInputStatesByteHidden() & 2) != 0);
            this.inputState2 = _tmp;
            return this.inputState2;
        }

        private int latitudeRawHidden;
        private int longitudeRawHidden;
        private int batteryVoltageRawHidden;
        private int gnssAndTemperatureByteHidden;
        private int speedAccuracyInputStatesByteHidden;
        private ForkliftLocationPacket _root;
        private ForkliftLocationPacket _parent;

        /**
         * Raw latitude value, divide by 100000 to get actual latitude.
         */
        public int latitudeRawHidden() {
            return latitudeRawHidden;
        }

        /**
         * Raw longitude value, divide by 100000 to get actual longitude.
         */
        public int longitudeRawHidden() {
            return longitudeRawHidden;
        }

        /**
         * Raw battery voltage, convert to actual voltage by adding 200 and dividing by 100.
         */
        public int batteryVoltageRawHidden() {
            return batteryVoltageRawHidden;
        }

        /**
         * Byte containing GNSS fix indicator and raw temperature.
         */
        public int gnssAndTemperatureByteHidden() {
            return gnssAndTemperatureByteHidden;
        }

        /**
         * Byte containing speed, HDOP accuracy, and input states.
         */
        public int speedAccuracyInputStatesByteHidden() {
            return speedAccuracyInputStatesByteHidden;
        }

        public ForkliftLocationPacket _root() {
            return _root;
        }

        public ForkliftLocationPacket _parent() {
            return _parent;
        }
    }

    public static class StopFrame extends KaitaiStruct {
        public static StopFrame fromFile(String fileName) throws IOException {
            return new StopFrame(new ByteBufferKaitaiStream(fileName));
        }

        public StopFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public StopFrame(KaitaiStream _io, ForkliftLocationPacket _parent) {
            this(_io, _parent, null);
        }

        public StopFrame(KaitaiStream _io, ForkliftLocationPacket _parent, ForkliftLocationPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }

        private void _read() {
            this.latitudeRawHidden = this._io.readS4be();
            this.longitudeRawHidden = this._io.readS4be();
            this.batteryVoltageRawHidden = this._io.readU1();
            this.gnssAndTemperatureByteHidden = this._io.readU1();
            this.speedAccuracyInputStatesByteHidden = this._io.readU1();
        }

        private Integer accuracy;

        /**
         * HDOP accuracy level derived from bits 3 and 2.
         */
        public Integer accuracy() {
            if (this.accuracy != null) {
                return this.accuracy;
            }
            int _tmp = (int) (((speedAccuracyInputStatesByteHidden() >> 2) & 3));
            this.accuracy = _tmp;
            return this.accuracy;
        }

        private Double batteryVoltage;

        public Double batteryVoltage() {
            if (this.batteryVoltage != null) {
                return this.batteryVoltage;
            }
            double _tmp = (double) (((batteryVoltageRawHidden() + 200) / 100.0));
            this.batteryVoltage = _tmp;
            return this.batteryVoltage;
        }

        private Boolean inputState1;

        /**
         * Input state 1, true if active.
         */
        public Boolean inputState1() {
            if (this.inputState1 != null) {
                return this.inputState1;
            }
            boolean _tmp = (boolean) ((speedAccuracyInputStatesByteHidden() & 1) != 0);
            this.inputState1 = _tmp;
            return this.inputState1;
        }

        private Integer temperature;

        /**
         * Temperature in Celsius, calculated from the byte value.
         */
        public Integer temperature() {
            if (this.temperature != null) {
                return this.temperature;
            }
            int _tmp = (int) (((gnssAndTemperatureByteHidden() & 127) - 40));
            this.temperature = _tmp;
            return this.temperature;
        }

        private Double latitude;

        public Double latitude() {
            if (this.latitude != null) {
                return this.latitude;
            }
            double _tmp = (double) ((latitudeRawHidden() / 100000.0));
            this.latitude = _tmp;
            return this.latitude;
        }

        private Double longitude;

        public Double longitude() {
            if (this.longitude != null) {
                return this.longitude;
            }
            double _tmp = (double) ((longitudeRawHidden() / 100000.0));
            this.longitude = _tmp;
            return this.longitude;
        }

        private Boolean gnssFix;

        /**
         * GNSS fix status; true if fix is valid.
         */
        public Boolean gnssFix() {
            if (this.gnssFix != null) {
                return this.gnssFix;
            }
            boolean _tmp = (boolean) ((gnssAndTemperatureByteHidden() & 128) != 0);
            this.gnssFix = _tmp;
            return this.gnssFix;
        }

        private Integer speed;

        /**
         * Speed in km/h, calculated from the upper 4 bits.
         */
        public Integer speed() {
            if (this.speed != null) {
                return this.speed;
            }
            int _tmp = (int) ((((speedAccuracyInputStatesByteHidden() >> 4) & 15) * 10));
            this.speed = _tmp;
            return this.speed;
        }

        private Boolean inputState2;

        /**
         * Input state 2, true if active.
         */
        public Boolean inputState2() {
            if (this.inputState2 != null) {
                return this.inputState2;
            }
            boolean _tmp = (boolean) ((speedAccuracyInputStatesByteHidden() & 2) != 0);
            this.inputState2 = _tmp;
            return this.inputState2;
        }

        private int latitudeRawHidden;
        private int longitudeRawHidden;
        private int batteryVoltageRawHidden;
        private int gnssAndTemperatureByteHidden;
        private int speedAccuracyInputStatesByteHidden;
        private ForkliftLocationPacket _root;
        private ForkliftLocationPacket _parent;

        /**
         * Raw latitude value, divide by 100000 to get actual latitude.
         */
        public int latitudeRawHidden() {
            return latitudeRawHidden;
        }

        /**
         * Raw longitude value, divide by 100000 to get actual longitude.
         */
        public int longitudeRawHidden() {
            return longitudeRawHidden;
        }

        /**
         * Raw battery voltage, convert to actual voltage by adding 200 and dividing by 100.
         */
        public int batteryVoltageRawHidden() {
            return batteryVoltageRawHidden;
        }

        /**
         * Byte containing GNSS fix indicator and raw temperature.
         */
        public int gnssAndTemperatureByteHidden() {
            return gnssAndTemperatureByteHidden;
        }

        /**
         * Byte containing speed, HDOP accuracy, and input states.
         */
        public int speedAccuracyInputStatesByteHidden() {
            return speedAccuracyInputStatesByteHidden;
        }

        public ForkliftLocationPacket _root() {
            return _root;
        }

        public ForkliftLocationPacket _parent() {
            return _parent;
        }
    }

    public static class CyclicFrame extends KaitaiStruct {
        public static CyclicFrame fromFile(String fileName) throws IOException {
            return new CyclicFrame(new ByteBufferKaitaiStream(fileName));
        }

        public CyclicFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public CyclicFrame(KaitaiStream _io, ForkliftLocationPacket _parent) {
            this(_io, _parent, null);
        }

        public CyclicFrame(KaitaiStream _io, ForkliftLocationPacket _parent, ForkliftLocationPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }

        private void _read() {
            this.latitudeRawHidden = this._io.readS4be();
            this.longitudeRawHidden = this._io.readS4be();
            this.batteryVoltageRawHidden = this._io.readU1();
            this.gnssAndTemperatureByteHidden = this._io.readU1();
            this.speedAccuracyInputStatesByteHidden = this._io.readU1();
        }

        private Integer accuracy;

        /**
         * HDOP accuracy level derived from bits 3 and 2.
         */
        public Integer accuracy() {
            if (this.accuracy != null) {
                return this.accuracy;
            }
            int _tmp = (int) (((speedAccuracyInputStatesByteHidden() >> 2) & 3));
            this.accuracy = _tmp;
            return this.accuracy;
        }

        private Double batteryVoltage;

        public Double batteryVoltage() {
            if (this.batteryVoltage != null) {
                return this.batteryVoltage;
            }
            double _tmp = (double) (((batteryVoltageRawHidden() + 200) / 100.0));
            this.batteryVoltage = _tmp;
            return this.batteryVoltage;
        }

        private Boolean inputState1;

        /**
         * Input state 1, true if active.
         */
        public Boolean inputState1() {
            if (this.inputState1 != null) {
                return this.inputState1;
            }
            boolean _tmp = (boolean) ((speedAccuracyInputStatesByteHidden() & 1) != 0);
            this.inputState1 = _tmp;
            return this.inputState1;
        }

        private Integer temperature;

        /**
         * Temperature in Celsius, calculated from the byte value.
         */
        public Integer temperature() {
            if (this.temperature != null) {
                return this.temperature;
            }
            int _tmp = (int) (((gnssAndTemperatureByteHidden() & 127) - 40));
            this.temperature = _tmp;
            return this.temperature;
        }

        private Double latitude;

        public Double latitude() {
            if (this.latitude != null) {
                return this.latitude;
            }
            double _tmp = (double) ((latitudeRawHidden() / 100000.0));
            this.latitude = _tmp;
            return this.latitude;
        }

        private Double longitude;

        public Double longitude() {
            if (this.longitude != null) {
                return this.longitude;
            }
            double _tmp = (double) ((longitudeRawHidden() / 100000.0));
            this.longitude = _tmp;
            return this.longitude;
        }

        private Boolean gnssFix;

        /**
         * GNSS fix status; true if fix is valid.
         */
        public Boolean gnssFix() {
            if (this.gnssFix != null) {
                return this.gnssFix;
            }
            boolean _tmp = (boolean) ((gnssAndTemperatureByteHidden() & 128) != 0);
            this.gnssFix = _tmp;
            return this.gnssFix;
        }

        private Integer speed;

        /**
         * Speed in km/h, calculated from the upper 4 bits.
         */
        public Integer speed() {
            if (this.speed != null) {
                return this.speed;
            }
            int _tmp = (int) ((((speedAccuracyInputStatesByteHidden() >> 4) & 15) * 10));
            this.speed = _tmp;
            return this.speed;
        }

        private Boolean inputState2;

        /**
         * Input state 2, true if active.
         */
        public Boolean inputState2() {
            if (this.inputState2 != null) {
                return this.inputState2;
            }
            boolean _tmp = (boolean) ((speedAccuracyInputStatesByteHidden() & 2) != 0);
            this.inputState2 = _tmp;
            return this.inputState2;
        }

        private int latitudeRawHidden;
        private int longitudeRawHidden;
        private int batteryVoltageRawHidden;
        private int gnssAndTemperatureByteHidden;
        private int speedAccuracyInputStatesByteHidden;
        private ForkliftLocationPacket _root;
        private ForkliftLocationPacket _parent;

        /**
         * Raw latitude value, divide by 100000 to get actual latitude.
         */
        public int latitudeRawHidden() {
            return latitudeRawHidden;
        }

        /**
         * Raw longitude value, divide by 100000 to get actual longitude.
         */
        public int longitudeRawHidden() {
            return longitudeRawHidden;
        }

        /**
         * Raw battery voltage, convert to actual voltage by adding 200 and dividing by 100.
         */
        public int batteryVoltageRawHidden() {
            return batteryVoltageRawHidden;
        }

        /**
         * Byte containing GNSS fix indicator and raw temperature.
         */
        public int gnssAndTemperatureByteHidden() {
            return gnssAndTemperatureByteHidden;
        }

        /**
         * Byte containing speed, HDOP accuracy, and input states.
         */
        public int speedAccuracyInputStatesByteHidden() {
            return speedAccuracyInputStatesByteHidden;
        }

        public ForkliftLocationPacket _root() {
            return _root;
        }

        public ForkliftLocationPacket _parent() {
            return _parent;
        }
    }

    public static class UnsupportedFrame extends KaitaiStruct {
        public static UnsupportedFrame fromFile(String fileName) throws IOException {
            return new UnsupportedFrame(new ByteBufferKaitaiStream(fileName));
        }

        public UnsupportedFrame(KaitaiStream _io) {
            this(_io, null, null);
        }

        public UnsupportedFrame(KaitaiStream _io, ForkliftLocationPacket _parent) {
            this(_io, _parent, null);
        }

        public UnsupportedFrame(KaitaiStream _io, ForkliftLocationPacket _parent, ForkliftLocationPacket _root) {
            super(_io);
            this._parent = _parent;
            this._root = _root;
            _read();
        }

        private void _read() {
            this.unsupportedData = this._io.readBytesFull();
        }

        private byte[] unsupportedData;
        private ForkliftLocationPacket _root;
        private ForkliftLocationPacket _parent;

        /**
         * Unsupported data payload
         */
        public byte[] unsupportedData() {
            return unsupportedData;
        }

        public ForkliftLocationPacket _root() {
            return _root;
        }

        public ForkliftLocationPacket _parent() {
            return _parent;
        }
    }

    private int frameCode;
    private KaitaiStruct payload;
    private ForkliftLocationPacket _root;
    private KaitaiStruct _parent;

    /**
     * Frame code
     */
    public int frameCode() {
        return frameCode;
    }

    public KaitaiStruct payload() {
        return payload;
    }

    public ForkliftLocationPacket _root() {
        return _root;
    }

    public KaitaiStruct _parent() {
        return _parent;
    }
}
