package ch.cern.nile.app;

import java.util.Collections;
import java.util.Map;

import com.google.gson.JsonObject;

import ch.cern.nile.app.generated.ForkliftLocationPacket;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;


public final class ForkliftLocationStream extends LoraDecoderStream<ForkliftLocationPacket> {

    public ForkliftLocationStream() {
        super(ForkliftLocationPacket.class, true, false, false);
    }

    @Override
    public Map<String, Object> enrichCustomFunction(final Map<String, Object> inputMap, final JsonObject metadataJson) {
        Map<String, Object> outputMap = inputMap;
        if ((double) inputMap.get("latitude") == 0.0 && (double) inputMap.get("longitude") == 0.0
                || !(boolean) inputMap.get("gnssFix")) {
            outputMap = Collections.emptyMap();
        }
        return outputMap;
    }

}
