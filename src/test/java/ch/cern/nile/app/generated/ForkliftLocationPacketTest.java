package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

class ForkliftLocationPacketTest {

    private static final JsonElement START_FRAME_COORDS_0 = TestUtils.getDataAsJsonElement("hAAAAAAAAAAAmDsN"); // 0x84

    private static final JsonElement START_FRAME = TestUtils.getDataAsJsonElement("hABGil8ACT1IlLoF"); // 0x84

    private static final JsonElement STOP_FRAME = TestUtils.getDataAsJsonElement("gABGlOUACTxEnLUD"); // 0x8

    private static final JsonElement CYCLIC_FRAME = TestUtils.getDataAsJsonElement("EABGjGsACTppnr8H"); // 0x10

    private static final JsonElement UNSUPPORTED_FRAME = TestUtils.getDataAsJsonElement("EAAhwAAAQxMBAA==");


    @Test
    void givenStartFrameWithLatLon0_whenDecoding_thenCorrectlyDecodesMessageData() {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(START_FRAME_COORDS_0, ForkliftLocationPacket.class);
        assertEquals(3.52, packet.get("batteryVoltage"));
        assertEquals(19, packet.get("temperature"));
        assertEquals(3, packet.get("accuracy"));
        assertEquals(0, packet.get("speed"));
        assertEquals(0.0, packet.get("latitude"));
        assertEquals(0.0, packet.get("longitude"));
        assertFalse((Boolean) packet.get("gnssFix"));
        assertFalse((Boolean) packet.get("inputState2"));
        assertTrue((Boolean) packet.get("inputState1"));
    }


    @Test
    void givenStartFrame_whenDecoding_thenCorrectlyDecodesMessageData() {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(START_FRAME, ForkliftLocationPacket.class);
        assertEquals(3.48, packet.get("batteryVoltage"));
        assertEquals(18, packet.get("temperature"));
        assertEquals(1, packet.get("accuracy"));
        assertEquals(0, packet.get("speed"));
        assertEquals(46.22943, packet.get("latitude"));
        assertEquals(6.05512, packet.get("longitude"));
        assertTrue((Boolean) packet.get("gnssFix"));
        assertFalse((Boolean) packet.get("inputState2"));
        assertTrue((Boolean) packet.get("inputState1"));
    }

    @Test
    void givenStopFrame_whenDecoding_thenCorrectlyDecodesMessageData() {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(STOP_FRAME, ForkliftLocationPacket.class);
        assertEquals(3.56, packet.get("batteryVoltage"));
        assertEquals(13, packet.get("temperature"));
        assertEquals(0, packet.get("accuracy"));
        assertEquals(0, packet.get("speed"));
        assertEquals(46.25637, packet.get("latitude"));
        assertEquals(6.05252, packet.get("longitude"));
        assertTrue((Boolean) packet.get("gnssFix"));
        assertTrue((Boolean) packet.get("inputState2"));
        assertTrue((Boolean) packet.get("inputState1"));
    }

    @Test
    void givenCyclicFrame_whenDecoding_thenCorrectlyDecodesMessageData() {
        Map<String, Object> packet = KaitaiPacketDecoder.decode(CYCLIC_FRAME, ForkliftLocationPacket.class);
        assertEquals(3.58, packet.get("batteryVoltage"));
        assertEquals(23, packet.get("temperature"));
        assertEquals(1, packet.get("accuracy"));
        assertEquals(0, packet.get("speed"));
        assertEquals(46.23467, packet.get("latitude"));
        assertEquals(6.04777, packet.get("longitude"));
        assertTrue((Boolean) packet.get("gnssFix"));
        assertTrue((Boolean) packet.get("inputState2"));
        assertTrue((Boolean) packet.get("inputState1"));
    }

    @Test
    void givenUnsupportedFrame_whenDecoding_thenThrowsDecodingException() {
        assertThrows(
                DecodingException.class,
                () -> KaitaiPacketDecoder.decode(UNSUPPORTED_FRAME, ForkliftLocationPacket.class));
    }

}
