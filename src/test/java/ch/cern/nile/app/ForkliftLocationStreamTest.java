package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.NoSuchElementException;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.common.streams.AbstractStream;
import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

class ForkliftLocationStreamTest extends StreamTestBase {

    private static final JsonObject START_FRAME_COORDS_0 = TestUtils.loadRecordAsJson("data/start_frame_coords_0.json");
    private static final JsonObject START_FRAME = TestUtils.loadRecordAsJson("data/start_frame.json");
    private static final JsonObject STOP_FRAME = TestUtils.loadRecordAsJson("data/stop_frame.json");
    private static final JsonObject CYCLIC_FRAME = TestUtils.loadRecordAsJson("data/cyclic_frame.json");

    @Override
    public AbstractStream createStreamInstance() {
        return new ForkliftLocationStream();
    }

    @Test
    void givenStartFrameWithCoords0_whenDecoding_thenNoOutputRecordIsCreated() {
        pipeRecord(START_FRAME_COORDS_0);
        assertThrows(NoSuchElementException.class, this::readRecord);
    }

    @Test
    void givenStartFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(START_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertEquals(3.48, outputRecord.value().get("batteryVoltage").getAsDouble());
        assertEquals(46.22943, outputRecord.value().get("latitude").getAsDouble());
        assertEquals(6.05512, outputRecord.value().get("longitude").getAsDouble());
        assertEquals(18, outputRecord.value().get("temperature").getAsInt());
        assertEquals(1, outputRecord.value().get("accuracy").getAsInt());
        assertEquals(0, outputRecord.value().get("speed").getAsInt());
        assertEquals("CH-01237", outputRecord.value().get("deviceName").getAsString());
        assertEquals("a0c02f1237f10000", outputRecord.value().get("devEUI").getAsString());
        assertEquals("22", outputRecord.value().get("applicationID").getAsString());
        assertEquals("lora-EN-forklift-location", outputRecord.value().get("applicationName").getAsString());
        assertEquals("StartFrame", outputRecord.value().get("frameType").getAsString());
        assertNotNull(outputRecord.value().get("timestamp"), "timestamp is null");
    }

    @Test
    void givenStopFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(STOP_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertEquals(3.56, outputRecord.value().get("batteryVoltage").getAsDouble());
        assertEquals(46.25637, outputRecord.value().get("latitude").getAsDouble());
        assertEquals(6.05252, outputRecord.value().get("longitude").getAsDouble());
        assertEquals(0, outputRecord.value().get("accuracy").getAsInt());
        assertEquals("CRLV-00298", outputRecord.value().get("deviceName").getAsString());
        assertTrue(outputRecord.value().get("gnssFix").getAsBoolean());
        assertEquals(0, outputRecord.value().get("speed").getAsInt());
        assertEquals("a0c03f0298f10000", outputRecord.value().get("devEUI").getAsString());
        assertTrue(outputRecord.value().get("inputState2").getAsBoolean());
        assertTrue(outputRecord.value().get("inputState1").getAsBoolean());
        assertEquals(13, outputRecord.value().get("temperature").getAsInt());
        assertEquals("StopFrame", outputRecord.value().get("frameType").getAsString());
        assertEquals("22", outputRecord.value().get("applicationID").getAsString());
        assertEquals("lora-EN-forklift-location", outputRecord.value().get("applicationName").getAsString());
        assertNotNull(outputRecord.value().get("timestamp"), "timestamp is null");
    }

    @Test
    void givenCyclicFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(CYCLIC_FRAME);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();
        assertEquals(3.58, outputRecord.value().get("batteryVoltage").getAsDouble());
        assertEquals(46.23467, outputRecord.value().get("latitude").getAsDouble());
        assertEquals(6.04777, outputRecord.value().get("longitude").getAsDouble());
        assertEquals(1, outputRecord.value().get("accuracy").getAsInt());
        assertEquals("CRLV-00298", outputRecord.value().get("deviceName").getAsString());
        assertTrue(outputRecord.value().get("gnssFix").getAsBoolean());
        assertEquals(0, outputRecord.value().get("speed").getAsInt());
        assertEquals("a0c03f0298f10000", outputRecord.value().get("devEUI").getAsString());
        assertTrue(outputRecord.value().get("inputState2").getAsBoolean());
        assertTrue(outputRecord.value().get("inputState1").getAsBoolean());
        assertEquals(23, outputRecord.value().get("temperature").getAsInt());
        assertEquals("CyclicFrame", outputRecord.value().get("frameType").getAsString());
        assertEquals("22", outputRecord.value().get("applicationID").getAsString());
        assertEquals("lora-EN-forklift-location", outputRecord.value().get("applicationName").getAsString());
        assertNotNull(outputRecord.value().get("timestamp"), "timestamp is null");
    }

}
